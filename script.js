/*
 * Cyclic movement script.
 *
 * Does cyclic movement between two border points with set values of acceleration,
 * deceleration and top speed.
 */

var LIMIT_X = 5000;
var LIMIT_Y = 5000;

var SPEED_MAX = 300;   // Maximum movement speed in steps / second
var ACCEL_MAX = 1000;   // Acceleration value in steps / second^2
var DECEL_MAX = 2000;   // Deceleration value in steps / second^2

var FILE = "fishnet.csv"

//=========================================

var PATH = "C:/Users/operator/Documents/csv/"

var FILENAME = PATH + FILE;


function echo( logstr )
{
   log(logstr, 2);
}

function set_extio( axis, bit )
{
  var settings = axis.get_extio_settings();
  settings.EXTIOModeFlags = bit ? EXTIO_SETUP_MODE_OUT_ON : EXTIO_SETUP_MODE_OUT_OFF;
  axis.set_extio_settings( settings );
}

function set_speed( axis, speed )
{
  speed = Math.abs( speed );
  var speed_int =  Math.floor( speed );
  var speed_256 = Math.floor( ( speed - speed_int )*256);
  var m = axis.get_move_settings();

  m.Speed = speed_int;
  m.uSpeed = speed_256;
  echo( "Set speed:" + speed + " -> " + speed_int + " " + speed_256 + "/256" );

  axis.set_move_settings(m);
}

function move_to( x_axis, y_axis, coord_x, coord_y )
{
 // if( coord_x < 0 ) { coord_x = 0; }
 // if( coord_x > LIMIT_X ) { coord_x = LIMIT_X; }

 // if( coord_y < 0 ) { coord_y = 0; }
 // if( coord_y > LIMIT_Y ) { coord_y = LIMIT_Y; }

  var GETS_X = x_axis.get_status();
  var GETS_Y = y_axis.get_status();

  var cur_x_int = GETS_X.CurPosition;
  var cur_y_int = GETS_Y.CurPosition;
  var cur_x_256 = GETS_X.uCurPosition;
  var cur_y_256 = GETS_Y.uCurPosition;

  var sx = Math.abs( cur_x_int + (cur_x_256/256.00) - coord_x );
  var sy = Math.abs( cur_y_int + (cur_y_256/256.00) - coord_y );
  var s = Math.sqrt( sx*sx + sy*sy );
  var t = s/SPEED_MAX;
  
  if( t > 0 )
  {
      var vx = sx/t;
      var vy = sy/t;
      set_speed( x_axis, vx );
      set_speed( y_axis, vy );
  }

  var coord_x_int =  Math.floor( coord_x );
  var coord_x_256 = Math.floor( ( coord_x - coord_x_int )*256);

  var coord_y_int =  Math.floor( coord_y );
  var coord_y_256 = Math.floor( ( coord_y - coord_y_int )*256);

  //var speed_x = 
  /*
  var m = axis.get_move_settings(); // read movement settings from the controller
  m.Speed = speed; // set movement speed
  m.Accel = accel; // set acceleration
  m.Decel = decel; // set deceleration
  axis.set_move_settings(m); // write movement settings into the controlle
  */

  //var GETS_X = x_axis.get_status();
  //var GETS_Y = y_axis.get_status();

  echo( "cur x = " + cur_x_int + "_" + cur_x_256 + "/256 " + "cur y = " + cur_y_int + "_" + cur_y_256 + "/256 "  );

  if (!(GETS_X.MvCmdSts & MVCMD_RUNNING) && ( cur_x_int != coord_x_int || cur_x_256 != coord_x_256 ))
  {
    x_axis.command_move( coord_x_int, coord_x_256 );   // move towards another border
    echo("Move X -> " + coord_x );
  }
  if (!(GETS_Y.MvCmdSts & MVCMD_RUNNING) && ( cur_y_int != coord_y_int || cur_y_256 != coord_y_256))
  {
    y_axis.command_move( coord_y_int, coord_y_256 );   // move towards another border
    echo("Move Y -> " + coord_y ); 
 }
}

var axes = [];
var number_of_axes = 0;
var last_serial = 0;

echo( "Test" );

while (serial = get_next_serial(last_serial))   // Get next serial number and repeat for each axes
{
  axes[number_of_axes] = new_axis(serial);
  echo("Found axis: " + number_of_axes + " with serial number: " + serial);
  //echo(serial);
  number_of_axes++;
  last_serial = serial;
}

for (var i = 0; i < number_of_axes; i++)
{
  axis_configure(axes[i]);
}

var axis_x =  axes[ 1 ];
var axis_y =  axes[ 0 ];
var laser_ctrl =  axes[ 0 ];

set_extio( laser_ctrl, 0 ); // laser off

//axis_x.command_move( -10, 100 );

//var tt = axis_x.get_engine_settings();
//echo( "antiply is: " + ( tt.EngineFlags ).toString() );

//tt.EngineFlags = tt.EngineFlags - 8;
//axis_x.set_engine_settings(tt);

//var pp = axis_x.get_engine_settings();
//echo( "antiply is: " + ( pp.EngineFlags ).toString() );

//set_speed( axis_x, 16.5 );
//axis_x.command_move( 200, 0 );

//while(true) {}

try
{
	if( SPEED_MAX > 1200 ) {
		SPEED_MAX = 1200;
		echo( "Speed limit set to 1200" );	
	}

	var f = new_file( FILENAME ); // Choose a file name and path; this script uses a file from examples in the installation directory
	
  const f_size = f.size()

  if(f_size > 0) {
    echo("Opening " + FILENAME + " , size: " + f_size + " bytes")
    f.open(); // Open a file
  } else {
    throw "Script aborted: File size: " + f_size + " bytes, check the file exists";
  }

	var ar = "", x, y;
	var laser_on = 0, pulse_time = 0;
	while ( fstr = f.read(4096) )
	{ // Read file contents string by string, assuming each string is less than 4KiB long
		echo("Read block: " + fstr);
		fstr = fstr.replace(/,/g, "."); //replace commas by dots
		//echo( fstr );
		ar = fstr.split(';'); // Split the string into substrings with comma as a separator; the result is anarray of strings
		x = parseFloat( ar[0] ); // Variable assignment
		y = parseFloat( ar[1] ); // Variable assignment
		laser_on = parseInt( ar[2] );
		pulse_time = parseInt( ar[3] );
		echo( "Moving to coordinate " + x + " : " + y + " : Laser: " + laser_on + " : Pulse: " + pulse_time  ); // Log the event
		//axis.command_move(x); // Move to the position
		move_to( axis_x, axis_y, x,y  );

		axis_x.command_wait_for_stop(10); // Wait until the movement is complete
		axis_y.command_wait_for_stop(10); // Wait until the movement is complete
	
		if( isNaN(pulse_time) ) {
			if( laser_on != 0 ) {
				set_extio( laser_ctrl, 1 ); // laser FIRE
			} else {
				set_extio( laser_ctrl, 0 ); // laser STOP
			}
		} else if( laser_on != 0 ) {
			set_extio( laser_ctrl, 1 );
			msleep(pulse_time); // milliseconds
			set_extio( laser_ctrl, 0 );
		}
	
		//log( "Waiting for " + 100 + " ms" ); // Log the event
		//msleep(100); // Wait for the specified amount of time
	}
	set_extio( laser_ctrl, 0 ); // laser STOP

	echo( "The end." );
	f.close(); // Close the file
	axis_configure( axis_x );
	axis_configure( axis_y );
} catch(err) {
	
	set_extio( laser_ctrl, 0 ); // laser STOP
	axis_configure( axis_x );
	axis_configure( axis_y );
	echo( "Exception: " + err );
}

/*
while (1)
{
  //msleep(100);
  //set_extio( axes[0], 1 );
  //msleep(100);
  //set_extio( axes[0], 0 );
 
  move_to( axes[0], axes[1], 1000, 2000 );
  break;
}


while (0)
{
  for (var i = 0; i < number_of_axes; i++)
  {
    go_first_border(axes[i]);
    go_second_border(axes[i]);
  }

  msleep(100);

}
*/

function axis_configure(axis)
{
  var speed = SPEED_MAX;   // Maximum movement speed in steps / second
  var accel = ACCEL_MAX;   // Acceleration value in steps / second^2
  var decel = DECEL_MAX;   // Deceleration value in steps / second^2

  axis.command_stop(); // send STOP command (does immediate stop)
  //axis.command_zero(); // send ZERO command (sets current position and encoder value to zero)
  var m = axis.get_move_settings(); // read movement settings from the controller
  m.Speed = speed; // set movement speed
  m.Accel = accel; // set acceleration
  m.Decel = decel; // set deceleration
  axis.set_move_settings(m); // write movement settings into the controller
  
  // setup extio
  var settings = axis.get_extio_settings();
  settings.EXTIOSetupFlags = EXTIO_SETUP_OUTPUT; // set to output
  settings.EXTIOModeFlags = EXTIO_SETUP_MODE_OUT_OFF; // set to 0
  axis.set_extio_settings( settings );

  var f_setup = settings.EXTIOSetupFlags;
  var f_mode = settings.EXTIOModeFlags;

  echo( "axis_configure: Axis extio flags: " + f_setup + " : " + f_mode );
}

function go_first_border(axis)
{
  var first_border = 0; // first border coordinate in steps
  var GETS = axis.get_status();

  if (!(GETS.MvCmdSts & MVCMD_RUNNING) && (GETS.CurPosition != first_border))
  {
    axis.command_move(first_border);  // move towards one border
  }
}

function go_second_border(axis)
{
  var second_border = 6000; // second border coordinate in steps
  var GETS = axis.get_status();

  if (!(GETS.MvCmdSts & MVCMD_RUNNING) && (GETS.CurPosition != second_border))
  {
    axis.command_move(second_border);   // move towards another border
  }
}



