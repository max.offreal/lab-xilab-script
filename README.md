# Lab-XILab-script

## About
This script using for control 8SMC4-USB Multi Axis Configuration Unit by standa.  
It reads .csv file line by line and control stepping motors and GPIO output.

## CSV info
Fields separator: `;`
### Fields
`x` - coordinate X to move (float, example: `4032.34` or `32,22` or `1000`)  
`y` - coordinate Y to move (float, example: `1032.34` or `72,22` or `5000`)  
`laser_on` - activate GPIO, such as opening laser latch (integer, example: `1` or `0`)  
`pulse_time` - if defined and __laser_on__ greater than 0, it makes single pulse with __pulse_time__ milliseconds duration (integer, example: `600`)  

### Example
simple:
```
0	;	0	;	1
4800	;	0	;	0
4800	;	32	;	1
0	;	32	;	0
```

with pulse_time:
```
0	;	0	;	1   ;   300
4800	;	0	;	1   ;   200
4800	;	32	;	1   ;   100
0	;	32	;	1   ;   50
```

### Filename
Change variables `FILE` and `PATH` in script to specify which .csv file to open:
```
var FILE = "ring.csv"
var PATH = "C:/Users/offreal/Documents/csv/"
```

## Documentation:
XILab scripting: https://doc.xisupport.com/en/8smc5-usb/8SMCn-USB/Programming/XILab_scripts.html#brief-description-of-the-language  
Product specification: https://www.standa.lt/products/catalog/motorised_positioners?item=525  